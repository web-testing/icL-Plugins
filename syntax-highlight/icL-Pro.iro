#################################################################
## Iro
################################################################ 
##
## * Press Ctrl + '+'/'-' To Zoom in
## * Press Ctrl + S to save and recalculate... 
## * Documents are saved to web storage.
## * Only one save slot supported.
## * Matches cannot span lines.
## * Unicode chars must be defined in \u0000 to \uffff format.
## * All matches must be contained by a single group ( ... )
## * Look behinds not permitted, (?<= or (?<!
## * Look forwards are permitted (?= or (?!
## * Constants are defined as __my_const = (......)
## * The \= format allows unescaped regular expressions
## * Constants referenced by match \= $${__my_const}
## * Constants can reference other constants
## * You are free to delete all the default scopes.
## * Twitter : ainslec , Web: http://eeyo.io/iro
##
################################################################

name                   = icL_Pro
file_extensions []     = icL, icLPro;

################################################################
## Constants
################################################################

################################################################
## Styles
################################################################

styles [] {

.comment : style {
   color          = grey
   textmate_scope = comment
   pygments_scope = Comment
}

.keyword : style {
   color          = yellow
   textmate_scope = keyword.control
   pygments_scope = Keyword
}

.numeric : style {
   color          = red
   textmate_scope = constant.numeric
   pygments_scope = Number
}

.punctuation : style {
   color          = white
   textmate_scope = punctuation
   pygments_scope = Punctuation
}

.illegal : style {
   color          = red_2
   textmate_scope = invalid
   pygments_scope = Generic.Error
}

.metaType : style {
   color          = blue
   textmate_scope = meta.type
   pygments_scope = Keyword.Type
}

.metaClass : style {
   color          = orange
   textmate_scope = meta.class
   pygments_scope = Name.Class
}

.property : style {
   color          = light_green
   textmate_scope = variable.other.member
   pygments_scope = Name
   // Name.Property
}

.codeLiteral : style {
   color          = light_blue
   textmate_scope = entity.name.function
   pygments_scope = Name.Function
}

.modifier : style {
   color          = green
   textmate_scope = entity.name.tag
   pygments_scope = Keyword.Pseudo
}

.method  : style {
   color          = green
   textmate_scope = entity.name.function
   pygments_scope = Name.Function
}

.lvariable : style {
   color          = blue
   textmate_scope = variable.other
   pygments_scope = Name.Variable
}

.gvariable : style {
   color          = orange
   textmate_scope = variable.language
   pygments_scope = Name.Variable.Global
}

.string : style {
   color          = purple
   textmate_scope = string.quoted.single
   pygments_scope = String
}

.function : style {
   color          = pink
   textmate_scope = entity.name.function
   pygments_scope = Name.Function
}

.regex : style {
   color          = purple
   textmate_scope = string.regexp
   pygments_scope = String.Regex
}

.struct : style {
   color          = orange
   textmate_scope = entity.name.struct
   pygments_scope = Name.Variable.Instance
}

.enum : style {
   color          = purple
   textmate_scope = entity.name.enum
   pygments_scope = Name
   // Name.Property
}

.type : style {
   color          = blue
   textmate_scope = entity.name.type
   pygments_scope = Keyword.Type
}

.operator : style {
    color         = yellow
   textmate_scope = keyword.operator
   pygments_scope = Operator
}

}

#################################################
## Parse contexts
#################################################

contexts [] {

##############################################
## Main Context - Entry point context
##############################################

main : context {

   : pattern {
      regex          \= \b(now|new|if|else|for|filter|range|exists|while|do|any|emitter|assert|listen|wait|jammer|switch|case|lambda|prototype|extends|notify)\b
      styles []       = .keyword;
   }
   
   : pattern {
      regex      \= \b(icL|Log|Tab|Document|Import|true|false|Numbers|Types|Key|Alert|By|DSV|Sessions?|Windows?|Cookies|Tabs?|Files?|Make|Math|Wait|Mouse|Move|Stacks?|State|DB|Query|DBManager|Code|Signal|Datetime|Request|View)\b
      styles []   = .metaClass;
   }
   
   : pattern {
      regex      \= (\')(year|y|xPath|x|windows|window|width|visible|value|valid|url|typeName|typeId|tsv|title|texts?|teleport|tagName|tags?|tabs?|sum|sqrt2|source|silentMode|shift|session|selected|secure|second|scriptTimeout|screenshot|right|rects?|readOnly|rValue|quadratic|product|process|previous|pressTime|present|piDiv4|piDiv2|pi|path|partialLinkText|pageLoadTimeout|none|next|name|moveTime|month|minute|min|middle|max|log2e|log10e|ln2|ln10|linkText|link|linear|length|left|last|lValue|implicitTimeout|humanMode|httpOnly|hour|height|format|flashMode|first|expiry|enabled|empty|e|domain|document|day|current|cubic|ctrl|csv|css\-\w+|cssSelector|cookies|clickTime|clickable|capacity|bezier|attrs?\-\w+|alt|alert|2divSqrtPi|2divPi|1divSqrt2|1divPi)\b
      styles []   = .property, .property;
   }
   
   : pattern {
      regex      \= (\'prop\-)(wrap|willValidate|width|volume|videoWidth|videoHeight|valueAsNumber|value|validity|validationMessage|username|useMap|type|title|textLength|textContent|text|target|tagName|tHead|tFoot|tBodies|step|start|srclang|src|spellcheck|span|size|selectionStart|selectionEnd|selectionDirection|selectedOptions|selectedIndex|selected|seeking|search|scrollWidth|scrollTop|scrollLeft|scrollHeight|scope|rowSpan|rowIndex|reversed|required|rel|readyState|readOnly|protocol|previousElementSibling|preload|prefix|poster|position|port|playbackRate|placeholder|paused|pattern|pathname|password|parentElement|outerHTML|origin|options|offsetWidth|offsetTop|offsetParent|offsetLeft|offsetHeight|nodeValue|nodeType|nodeName|noValidate|noModule|nextElementSibling|networkState|naturalWidth|naturalHeight|name|muted|multiple|min|mediaGroup|media|maxLength|max|low|loop|localName|list|length|lastChild|lang|labels|label|kind|isMap|isContentEditable|isConnected|innerText|innerHTML|inert|index|indeterminate|id|httpEquiv|htmlFor|hreflang|href|hostname|host|high|hidden|height|hash|formTarget|formNoValidate|formMethod|formEnctype|formAction|form|firstChild|ended|enctype|encoding|elements|duration|draggable|download|disabled|disableRemotePlayback|dirName|dir|defer|defaultValue|defaultSelected|defaultPlaybackRate|defaultMuted|defaultChecked|default|dateTime|dataset|currentTime|currentSrc|crossOrigin|coords|controls|control|contentEditable|content|computedRole|computedName|complete|cols|colSpan|clientWidth|clientTop|clientLeft|clientHeight|className|cite|childNodes|checked|charset|cells|cellIndex|caption|baseURI|autoplay|autofocus|autocomplete|async|as|areas|alt|allowPaymentRequest|action|accessKeyLabel|accessKey|acceptCharset|accept|abbr)\b
      styles []   = .property, .property;
   }
   
   : pattern {
      regex      \= (\')([\w\-]+)
      styles []   = .punctuation, .property;
   }

   : inline_push {
      regex            \= (sql)({)
      styles []         = .codeLiteral, .punctuation;
      
      : pop {
         regex      \= (})
         styles []   = .punctuation;
      }
      
      : include "sqlCode" ;
   }
   
   : pattern {
      regex      \= (\.)(write|type|trim|toUpperCase|toUTC|toTimeZone|toPrev|toNext|toLowerCase|toLast|toFirst|tan|sync|switchToParent|switchToFrame|switchToDefault|switchTo|superClick|substring|state|stack|split|sort|sin|setup|setProcess|setAsUserScript|setAsPersistentUserScript|set|sendKeys|seek|secsTo|screenshot|save|runAsync|run|round|rightJustified|right|return|restoreProcess|restore|resetTime|replaceInStrings|replace|removeOne|removeLast|removeFirst|removeField|removeDuplicates|removeAt|removeAll|remove|refresh|radiansToDegrees|process|previous|prev|prepend|paste|parent|out|openSQLite|open|none|next|newAtEnd|new|move|mouseUp|mouseDown|minimize|min|mid|maximize|max|markTest|markStep|loadTSV|loadCSV|load|ln|listen|leftJustified|left|lastIndexOf|last|kill|keyUp|keyPress|keyDown|join|insertField|insert|info|indexOf|image|ignore|hover|hasField|handle|getRowsAffected|getLength|getField|getError|get|functions|fullscreen|forward|forceType|forceClick|focus|floor|first|findByTitle|find|filter|fastType|exp|exec|error|ensureRValue|endsWith|dismiss|destroy|deleteAll|delete|degreesToRadians|deactivate|daysTo|currentUTC|current|crossfire|createPath|createDir|create|count|cos|copy|continue|contains|connect|compare|closest|closeToRight|closeToLeft|closeOthers|closeByTitle|closeAll|close|clone|click|clear|child|ceil|break|beginsWith|back|atan|at|asin|applicate|append|all|addYears|addSecs|addMonths|addDescription|addDays|add|activate|acos|accept)\b
      styles []   = .method, .method;
   }
   
   : pattern {
      regex      \= \b(emit|slot)(?:(-\w+)?)\b
      styles []   = .keyword, .modifier;
   }
   
   : inline_push {
      regex      \= \b(css)((?:-all|-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation;
      
      : pop {
         regex      \= (\])
         styles []   = .punctuation;
      }
      
      : include "webElement" ;
   }
   
   : pattern {
      regex      \= \b(xpath)(:all|:try\d+m?s|:try|:wait\d+m?s)?(@\w*)?(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation, .string, .punctuation;
   }
   
   : pattern {
      regex      \= \b(xpath)((?:-all|-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation, .string, .punctuation;
   }
   
   : pattern {
      regex      \= \b(link)((?:-fragment|-try|-wait\d+m?s|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation, .string, .punctuation;
   }
   
   : pattern {
      regex      \= \b(links)((?:-fragment|-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation, .string, .punctuation;
   }
   
   : pattern {
      regex      \= \b(input)((?:-try|-wait\d+m?s)*)(@\w*)?(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation, .string, .punctuation;
   }
   
   : pattern {
      regex      \= \b(field)((?:-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation, .string, .punctuation;
   }
   
   : pattern {
      regex      \= \b(tag)((?:-\w+)??(?:-try|-wait\d+m?s|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation, .string, .punctuation;
   }
   
   : pattern {
      regex      \= \b(tags)((?:-\w+)??(?:-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .lvariable, .punctuation, .string, .punctuation;
   }
   
   : inline_push {
      regex            \= \b(js)(-value)?(@\w*)?({)
      styles []         = .codeLiteral, .modifier, .lvariable, .punctuation;
      
      : pop {
         regex      \= (})
         styles []   = .punctuation;
      }
      
      : include "jsCode" ;
   }
   
   : pattern {
      regex      \= \b(js)(-file)(\[)(.*?)(\])
      styles []   = .codeLiteral, .modifier, .punctuation, .string, .punctuation;
   }
   
   : pattern {
      regex      \= (\$\w+)
      styles []   = .function;
   }
   
   : pattern {
      regex      \= (@\w*)
      styles []   = .lvariable;
   }
   
   : pattern {
      regex      \= (#\w+)
      styles []   = .gvariable;
   }
   
   : pattern {
      regex      \= (#)
      styles []   = .lvariable;
   }
   
   : pattern {
      regex      \= (\{:)(\w+)\b
      styles []   = .punctuation, .property;
   }
   
   : pattern {
      regex      \= \b(-)(not|alive|ignore|ajax|try|[\dX]+m?s|alt|ever|[\dX]+times|reverse|max[\dX]+|min[\dX]+)\b
      styles []   = .modifier, .modifier;
   }
   
   : pattern {
      regex      \= \b(\d+\.?\d*?)\b
      styles []   = .numeric;
   }
   
   : pattern {
      regex      \= (//.*?//|/:.*?:/|/!.*?\!/|/".*?"/)(\w*)
      styles []   = .regex, .regex;
   }
   
   : pattern {
      regex      \= \b(sessions|windows|tabs|cookies|alert|tabs|css|xpath|links?|tags?|input|field)\b
      styles []   = .illegal;
   }
   
   : pattern {
      regex      \= \b(bool|int|double|string|list|element|set|item|object|void|regex|datetime|session|window|cookie|tab|document|file|query|database|databasemanager|code|js|sql|script|handler|any|type)\b
      styles []   = .metaType;
   }
   
   : inline_push {
      regex          \= (\")
      styles []       = .string;
      
      : pop {
         regex       \= (\")
         styles []    = .string;
      }
      
      : include "string" ;
   }
   
   : inline_push {
      regex            \= (```)
      styles []         = .comment;
      default_style     = .comment
      : pop {
         regex      \= (```)
         styles []   = .comment;
      }
   }
   
   : inline_push {
      regex            \= (``)
      styles []         = .comment;
      default_style     = .comment
      : pop {
         regex      \= ($)
         styles []   = .comment;
      }
   }
   
   : inline_push {
      regex            \= (`)
      styles []         = .comment;
      default_style     = .comment
      : pop {
         regex      \= (`)
         styles []   = .comment;
      }
   }
   
   : inline_push {
      regex            \= (\[)
      styles []         = .operator;
      : pop {
         regex      \= (\])
         styles []   = .operator;
      }
      
      : include "main" ;
      
      : pattern {
         regex      \= (\w+)
         styles []   = .property;
      }
   }
   
   : pattern {
      regex          \= (;)
      styles []       = .punctuation;
   }

   : pattern {
      regex          \= (:\!|::|:\*|:=|:\?|&|\||~|^|%|==|\!=|>=|<=|<>|<=>|><|>=<|<<|\!<|<\*|\!\*|\*\*|/'|\!|>|<|\+|\-|\*|/|\\|:|\(|\)|\=|\,|\[|\])
      styles []       = .operator;
   }
}

#################################################
## End of Contexts
#################################################

string : context {
   : pattern {
      regex      \= (\\.)
      styles []   = .string;
   }
   
   : pattern {
      regex      \= (.)
      styles []   = .string;
   }
}

sqlCode : context {
   : pattern {
      regex      \= \b(SELECT|FROM|WHERE|LIMIT|INSERT|INTO|VALUE)\b
      styles []   = .keyword;
   }
   
   : pattern {
      regex      \= (@?:[\w\_]+)
      styles []   = .lvariable;
   }
   
   : pattern {
      regex      \= (#:[\w\_]+)
      styles []   = .gvariable;
   }
   
   : pattern {
      regex      \= ([\w\_]+)(\s*\()
      styles []   = .function, .punctuation;
   }
}

webElement : context {
   : pattern {
      regex      \= (\.[\w\-\_]+)
      styles []   = .metaClass;
   }
   
   : pattern {
      regex      \= (\#[\w\-\_]+)
      styles []   = .struct;
   }
   
   : pattern {
      regex      \= (\:[\w\-\_]+)(\s*\()
      styles []   = .function, .punctuation;
   }
   
   : pattern {
      regex      \= (\:[\w\-\_]+)
      styles []   = .enum;
   }
   
   : pattern {
      regex      \= \b([\w\-\_]+)(=[^\w]?)([\w\-\_]+)
      styles []   = .property, .punctuation, .string;
   }
   
   : pattern {
      regex      \= \b(\d+)\b
      styles []   = .numeric;
   }
   
   : pattern {
      regex      \= \b([\w\-\_]+)\b
      styles []   = .type;
   }
   
   : inline_push {
      regex            \= (\[)
      styles []         = .punctuation;
      
      : pop {
         regex      \= (\])
         styles []   = .punctuation;
      }
      
      : include "webElement" ;
   }
}

jsCode : context {
   : pattern {
      regex      \= \b(abstract|arguments|await|boolean|break|byte|case|catch|char|class|const|continue|debugger|default|delete|do|double|else|enum|eval|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|let|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with|yield)\b
      styles []   = .keyword;
   }
   
   : pattern {
      regex      \= (\${[\w\_]+})
      styles []   = .function;
   }
   
   : pattern {
      regex      \= (\.[\w\_]+)(\s*\()
      styles []   = .function, .punctuation;
   }
   
   : pattern {
      regex      \= (\b[\w\_]+)(\s*\()
      styles []   = .function, .punctuation;
   }
   
   : pattern {
      regex      \= (\.[\w\_]+)
      styles []   = .property;
   }
   
   : pattern {
      regex      \= \b(window|document|crossfire)\b
      styles []   = .metaClass;
   }
   
   : inline_push {
      regex            \= ({)
      styles []         = .punctuation;
      
      : pop {
         regex      \= (})
         styles []   = .punctuation;
      }
      
      : include "jsCode" ;
      
   }
}

}
