import re

from pygments.lexer import RegexLexer, bygroups
from pygments.token import *

__all__ = ['IclLexer']

class IclLexer(RegexLexer):
    name = 'icL'
    aliases = ['icl']
    filenames = ['*.icL', '*.icL-Pro']
    
    flags = re.MULTILINE | re.DOTALL

    tokens = {

        'root': [

            (r'\b(now|new|if|else|for|filter|range|exists|while|do|any|emitter|assert|listen|wait|jammer|switch|case|lambda|prototype|extends|notify)\b',
                Keyword),
            
            (r'\b(sessions|windows|tabs|cookies|alert|tabs)\b', 
                Name.Label),

            (r'\b(icL|Log|Tab|Document|Import|true|false|Numbers|Types|Key|Alert|By|DSV|Sessions?|Windows?|Cookies|Tabs?|Files?|Make|Math|Wait|Mouse|Move|Stacks?|State|DB|Query|DBManager|Signal|Datetime|Request|View)\b', 
                Name.Class),

            (r'\'[\w\-\*]+', 
                Name.Property),

            (r'(sql)(\{)',
                bygroups(Name.Function,
                    Text),
                'sql_code'),

            (r'\.\w+', 
                Name.Function),

            (r'\b(emit|slot)(-\w+)?\b',
                bygroups(Keyword,
                    Name.Exception)),

            (r'\b(css)((?:-all|-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text),
                'web_element'),

            (r'\b(xpath)((?:-all|-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text,
                    String,
                    Text)),

            (r'\b(link)((?:-fragment|-try|-wait\d+m?s|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text,
                    String,
                    Text)),

            (r'\b(links)((?:-fragment|-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text,
                    String,
                    Text)),

            (r'\b(input)((?:-try|-wait\d+m?s)*)(@\w*)?(\[)(.*?)(\])',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text,
                    String,
                    Text)),

            (r'\b(field)((?:-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text,
                    String,
                    Text)),

            (r'\b(tag)((?:-\w+)??(?:-try|-wait\d+m?s|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text,
                    String,
                    Text)),

            (r'\b(tags)((?:-\w+)??(?:-try|-wait\d+m?s|-min\d+|-max\d+|-from\d\.\d+|-to\d\.\d+)*)(@\w*)?(\[)(.*?)(\])',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text,
                    String,
                    Text)),

            (r'\b(js)(-value)?(@\w+)?(\{)',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Name.Variable,
                    Text),
                'js_code'),

            (r'\b(js)(-file)(\[)(.*?)(\])',
                bygroups(Name.Function,
                    Keyword.Pseudo,
                    Text,
                    String,
                    Text)),

            (r'\$\w+', 
                Name.Function),

            (r'@\w*', 
                Name.Variable),

            (r'#\w+', 
                Name.Variable.Global),

            (r'#',
                Name.Variable),

            (r'(\b-)(not|alive|ignore|ajax|try|[\dX]+m?s|alt|ever|[\dX]+times|reverse|max[\dX]+|min[\dX]+)\b',
                bygroups(Keyword.Pseudo,
                    Keyword.Pseudo)),

            (r'\b(\d+)(\.\d+)?\b',
                Number),

            (r'(//.*?//|/:.*?:/|/\$.*?\$/|/".*?"/)(\w*)', 
                bygroups(String.Regex,
                    String.Symbol)),

            (r'\b(sessions|windows|tabs|cookies|alert|tabs|css|xpath|links?|tags?|input|field)\b',
                Name.Variable.Global),

            (r'\b(bool|int|double|string|list|element|set|item|object|void|regex|datetime|session|window|cookie|tab|document|file|query|database|databasemanager|code|js|sql|script|handler|any|type)\b',
                Keyword.Type),
            
            (r'\w+', Name.Property),

            (r'"',
                String,
                'string'),

            (r'`(``.*?```|`.*?$|.*?`)', 
                Comment),

            (r'.', Text)
        ],

        'string': [
            
            (r'\\.',
                String.Escape),

            (r'"', 
                String, 
                '#pop'),

            (r'.', String)
        ],

        'sql_code': [

            (r'\b(SELECT|FROM|WHERE|LIMIT|INSERT|INTO|VALUE)\b',
                Keyword),

            (r'@:[\w\_]+',
                Name.Variable),

            (r'#:[\w\_]+',
                Name.Variable.Global),

            (r'([\w\_]+)(\s*\()',
                bygroups(Name.Function,
                    Text)),

            (r'\d+(\.\d+)?',
                Number),

            (r'}',
                Text,
                '#pop'),

            (r'.', Text)
        ],

        'web_element': [

            (r'\.[\w\-\_]+',
                Name.Class),

            (r'#[\w\-\_]+',
                Name.Variable.Instance),

            (r'(\:[\w\-\_]+)(\s*\()',
                bygroups(Name.Function,
                    Text)),

            (r'\:[\w\-\_]+',
                Name.Property),

            (r'\b([\w\-\_]+)(=[^\w]?)([\w\-\_]+)',
                bygroups(Name.Attribute,
                    Text,
                    String)),

            (r'\b\d+(\.\d+)?\b',
                Number),

            (r'\b[\w\-\_]+\b',
                Keyword.Type),

            (r'\[',
                Text,
                '#push'),

            (r'\]',
                Text,
                '#pop'),

            (r'.', Text)
        ],

        'js_code': [

            (r'\b(abstract|arguments|await|boolean|break|byte|case|catch|char|class|const|continue|debugger|default|delete|do|double|else|enum|eval|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|let|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with|yield)\b',
                Keyword),

            (r'\$\{[\w\_]+\}',
                Name.Function),

            (r'(\.)([\w\_]+)(\s*\()',
                bygroups(Text,
                    Name.Function,
                    Text)),

            (r'(\.)([\w\_]+)',
                bygroups(Text,
                    Name.Property)),

            (r'\b(window|document|crossfire)\b',
                Name.Variable.Global),

            (r'"',
                String,
                'string'),

            (r'\{',
                Text,
                '#push'),

            (r'\}',
                Text,
                '#pop'),

            (r'.', Text)
        ]
    }
