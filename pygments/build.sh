#!/bin/bash

git clone https://github.com/pygments/pygments pygments;
cp lexers/icl.py pygments/pygments/lexers;
cp styles/icl.py pygments/pygments/styles;
cp styles/icldark.py pygments/pygments/styles;

cd pygments;
make mapfiles;
cd ..;

sudo cp -rf pygments/pygments/* /usr/lib/python3.8/site-packages/pygments;
rm -rf pygments;
